function defaultSettings(){
    if(parseInt(localStorage.currentLevelStored) >= 2){
        $("#introBlock1").css("display", "block");
        $(".introExit4").css("display", "block");
    }

    footerToogled = false;
    hitboxToogled = false;
    selectionInProgress = false;
    blockInMovement = false;
    debugMode = false;
    possibleScoreLoss = false;
    finishingAnim = false;
    buttonsAvailable = false;
    arrowsAvailable = false;
    currentLevel = 1;
    currentHelpPage = 1;
    movesCounter = 0;
    backMovesAvailable = 3;
    levelSeconds = 1;
    blockSpacing = 3;
    rightPanelWidth = 220;
    buttonCheckArray = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    reversion = new Array();

    mainColor = "rgb(82, 57, 206)";
    textColor = "rgb(215, 215, 215)";
    bgColor1 = "rgb(25, 25, 25)";
    bgColor2 = "rgb(17, 17, 17)";
    bgColor3 = "rgb(10, 10, 10)";
    targetBlockColor = "rgb(82, 57, 206)";
    blockColor = "rgb(45, 45, 45)";  

    if(localStorage.getItem("myColorTheme") === null){
          
    }else{
        var tempArr = new Array(7); 
        tempArr = JSON.parse(localStorage.myColorTheme);

        if(tempArr[0] != null){
            mainColor = tempArr[0];
            document.getElementById('g1').jscolor.fromString(mainColor);
            changeMainColor();
        }
        if(tempArr[1] != null){
            textColor = tempArr[1];
            document.getElementById('g2').jscolor.fromString(textColor);
            changeTextColor();
        }
        if(tempArr[2] != null){
            bgColor1 = tempArr[2];
            document.getElementById('g3').jscolor.fromString(bgColor1);
            changeBackgroundColor1();
        }
        if(tempArr[3] != null){
            bgColor2 = tempArr[3];
            document.getElementById('g4').jscolor.fromString(bgColor2);
            changeBackgroundColor2();
        }
        if(tempArr[4] != null){
            bgColor3 = tempArr[4];
            document.getElementById('g5').jscolor.fromString(bgColor3);
            changeBackgroundColor3();
        }
        if(tempArr[5] != null){
            targetBlockColor = tempArr[5];
            document.getElementById('g6').jscolor.fromString(targetBlockColor);
            changeTargetBlockColor();
        }
        if(tempArr[6] != null){
            blockColor = tempArr[6];
            document.getElementById('g7').jscolor.fromString(blockColor);
            changeBlockColor();
        }  
    }
    
    // xml has 2 second to response from this point
    xmlTimeOut = setInterval(function(){
        alert("XML load failed! Try Ctrl + F5 and check your network!");
    }, 3000); 

    load("countLevels");

    if(localStorage.isSaved == "yes"){
        $("#introBlock1").css("display", "block");
        $(".introExit4").css("display", "block");
    }

    // setting up displayed time
    updateTime();
    var dateTimer = setInterval(updateTime, 1000);

    menuSong = $("#menuSong")[0];
    menuSong.currentTime = 0;
    menuSong.volume = 0.4;
    menuSong.loop = true;
    menuSong.play();

    dragSound = $("#dragSound")[0];
    dragSound.currentTime = 0;
    dragSound.volume = 0.8;
    dragSound.loop = false;

    clickSound = $("#clickSound")[0];
    clickSound.currentTime = 0;
    clickSound.volume = 0.38;
    clickSound.loop = false;

    levelCompletedSound = $("#levelCompletedSound")[0];
    levelCompletedSound.currentTime = 0;
    levelCompletedSound.volume = 0.7;
    levelCompletedSound.loop = false;

    endSong = $("#endSong")[0];
    endSong.currentTime = 0;
    endSong.volume = 0.6;
    endSong.loop = false;
}

function toogleFooter(){
    if(buttonsAvailable == true && selectionInProgress == false){
        playDragSound();

        if(footerToogled == false){
            $("#footer").animate({bottom: '0px'}, 200);
            $("#toogleFooterButton").html("Debug&nbsp;&nbsp;&nbsp;&#8891;");
            footerToogled = true;
            toogleHitbox(true);
        }
        else{
            $("#footer").animate({bottom: '-103px'}, 200);
            $("#toogleFooterButton").html("Debug&nbsp;&nbsp;&nbsp;&#8892;");
            footerToogled = false;
            toogleHitbox(false);
        }
    }  
}

function toogleHitbox(param){
    if(param == true){
        $(".block").css("background-color", "rgba(255, 0, 0, 0.3)");
    }
    else{
        $(".block").css("background-color", "rgba(255, 0, 0, 0)");
    }  
}

function updateTime(){
    var d = new Date();
    $("#currentDateTime").html(d.toLocaleTimeString() + "<br>" + d.toLocaleDateString());
}

// together with changing size of block, i need to change also border-radius
function changeSizeOfBlock(x){
    sizeOfBlock = x;

    var radiusVal = Math.floor(sizeOfBlock / 7);

    css = ".blockInner, .targetBlockInner{border-radius: " + radiusVal + "px;}";
    style = document.createElement('style');
    
    if (style.styleSheet) {
        style.styleSheet.cssText = css;
    } else {
        style.appendChild(document.createTextNode(css));
    }
    
    document.getElementsByTagName('head')[0].appendChild(style);
}

function getBlockPos(value){
    return Math.round(value / sizeOfBlock);
}

function adjustUI(){
    var inputString;

    inputString = 600 + rightPanelWidth + 60;
    $("#articleIntro").css("width", inputString);
    $("#article").css("width", inputString);
    $(".stats").css("width", rightPanelWidth);
    $(".buttonsBox").css("width", rightPanelWidth);

    $(".introExit2").css("left", inputString - 1);
    $(".introExit4").css("left", inputString - 1);

    // adjust spaces between buttons for new .stats width
    var leftPosition = 30 + ((rightPanelWidth - 180) / 5); 
    inputString = 1 * leftPosition;
    $("#resetButton").css("left", inputString);
    inputString = 2 * leftPosition;
    $("#menuButton").css("left", inputString);
    inputString = 3 * leftPosition;
    $("#saveButton").css("left", inputString);
    inputString = 4 * leftPosition;
    $("#settingsButton").css("left", inputString);
}

function testCollisionArray(){
    var arr = "";
    
    console.log("Collision array:");

    for(var i = 0; i < mapSize; i++){
        for(var j = 0; j < mapSize; j++){
            arr += collisionArray[j][i] + " ";
        }

        arr += "\n";
    }

    console.log(arr);
}

function testExitLocationPosition(){
    var arr = "";

    console.log("Exit location position:");

    for(var i = 0; i < mapSize; i++){
        for(var j = 0; j < mapSize; j++){
            arr += exitLocationPosition[j][i] + " ";
        }

        arr += "\n";
    }

    console.log(arr);
}

function levelDone(obj, size, direction){
    var pos;
    var tempArr;

    localStorage.isSaved = "no";
    try{
        localStorage.removeItem(mySave);    
    }catch(error){}

    if(localStorage.getItem("bestScore") === null){
        tempArr = new Array(totalLevels);
        tempArr[currentLevel - 1] = currentScore;      
        localStorage.bestScore = JSON.stringify(tempArr);  
    }
    else{
        tempArr = JSON.parse(localStorage.bestScore);
        
        if(currentScore > tempArr[currentLevel - 1] || tempArr[currentLevel - 1] == null){
            tempArr[currentLevel - 1] = currentScore;
            localStorage.bestScore = JSON.stringify(tempArr);
        }
    }
    
    if(currentLevel <= totalLevels){
        currentLevel++;
    }
    
    if(currentLevel > parseInt(localStorage.currentLevelStored)){
        localStorage.currentLevelStored = currentLevel.toString();
    }
    
    if(parseInt(localStorage.currentLevelStored) >= 2){
        $("#introBlock1").css("display", "block");
        $(".introExit4").css("display", "block");
    }

    buttonsAvailable = false;
    finishingAnim = true;

    clearInterval(levelTimer);

    $(".horizontalDrag").draggable("disable").off("click");
    $(".verticalDrag").draggable("disable").off("click");
    $(".block").off("mouseover");

    levelCompletedSound.pause();
    levelCompletedSound.currentTime = 0;
    levelCompletedSound.play();

    if(direction == "left"){
        pos =  (-1) * ((size * sizeOfBlock) + sizeOfBlock);
        $(obj).animate({left: pos + 'px'}, 300, function(){
            levelCompletedDisplay();
        });
    }
    else if(direction == "right"){
        pos = (mapSize * sizeOfBlock) + sizeOfBlock;
        $(obj).animate({left: pos + 'px'}, 300, function(){
            levelCompletedDisplay();
        });
    }
    else if(direction == "top"){
        pos =  (-1) * ((size * sizeOfBlock) + sizeOfBlock);
        $(obj).animate({top: pos + 'px'}, 300, function(){
            levelCompletedDisplay();
        });
    }
    else if(direction == "bottom"){
        pos = pos = (mapSize * sizeOfBlock) + sizeOfBlock;
        $(obj).animate({top: pos + 'px'}, 300, function(){
            levelCompletedDisplay();
        });
    }   
}

function hasClass(element, myClass){ // return false if doesnt have
    return (' ' + element.className + ' ').indexOf(' ' + myClass + ' ') > -1;
}

function incMoveCounter(){
    $("#movesValue").html(++movesCounter);
    decScore(10);
}

function decScore(value){
    if(currentScore > 9){
        currentScore -= value;   
    } 
    else if(currentScore <= 9 && currentScore > 0 && value > 1){
        currentScore = 0;
    }
    else if(currentScore > 0){
        currentScore -= value;
    }

    if(possibleScoreLoss == true){
        $("#scoreValue").html(currentScore + 10);
    }
    else{
        $("#scoreValue").html(currentScore);
    } 
}

function fadeSwap(hide, show){
    $(hide).fadeTo(150, 0, function(){
        $(this).css("display", "none");
    });

    $(show).css("display", "block").fadeTo(150, 1);
}

function resetIntroButtons(){
    $("#introBlock1").css("left", "50%");
    $("#introBlock2").css("left", "50%");
    $("#introBlock3").css("left", "50%");
    $("#introBlock4").css("left", "50%");
    $("#helpBlock1").css("left", "50%");
}

function resetGameCompletedButtons(){
    $("#replayButton").css("left", "250px");
    $("#nextButton").css("left", "430px");    
}

function displayFooter(){
    footerToogled = false;

    $("#footer").css("display", "block");
    $("#footer").css("bottom", "-103px");
    $("#toogleFooterButton").html("Debug&nbsp;&nbsp;&nbsp;&#8892;");

    $("#footer").fadeTo(100, 1);
}

function hideFooter(){
    toogleHitbox(false);

    if(footerToogled == false){
        $("#footer").fadeTo(100, 0, function(){
            $("#footer").css("display", "none");
        });
    }
    else if(footerToogled == true){
        footerToogled = false;

        $("#footer").fadeTo(100, 0, function(){
            $("#footer").animate({bottom: '-103px'});
            $("#toogleFooterButton").html("Debug&nbsp;&nbsp;&nbsp;&#8892;");
            $("#footer").css("display", "none");
        });   
    }  
}

function masterCheck(){
    if(selectionInProgress == false && buttonsAvailable == true && blockInMovement == false){
        return true;
    }
    else{
        return false;
    }
}

function levelCompletedDisplay(){
    buttonsAvailable = false;
    clearInterval(levelTimer2);

    $(".levelCompletedHeading").html("LEVEL " + (currentLevel - 1) + " COMPLETED");

    var minutes = Math.floor(levelSeconds / 60);
    var seconds = levelSeconds - minutes * 60;

    if(seconds < 10){
        seconds = "0" + seconds;
    }

    $("#levelCompletedTimeValue").html(minutes + ":" + seconds);
    $("#levelCompletedScoreValue").html(currentScore);

    var tempArr = JSON.parse(localStorage.bestScore);
    $("#levelCompletedBestScoreValue").html(tempArr[currentLevel - 2]);
    $("#levelCompletedMovesValue").html(movesCounter);

    fadeSwap("#game", "#levelCompleted");
    hideFooter();
}

function updateReversion(obj, prevX, prevY){
    var i, j;

    reversion.length = 0;
    reversion = new Array(); 

    reversion.push(obj);
    reversion.push(prevX);
    reversion.push(prevY);

    //allocating
    reversion[3] = new Array(mapSize);
    
    for (j = 0; j < mapSize; j++) {
        reversion[3][j] = new Array(mapSize);
    }

    //adding current matrix
    for(i = 0; i < mapSize; i++){
        for(var j = 0; j < mapSize; j++){
            reversion[3][i][j] = collisionArray[i][j];
        }
    }
}

function cmp2DArrays(arr1, arr2){
    for(var i=0; i < mapSize; i++){
        for(var j=0; j < mapSize; j++){
            if(arr1[i][j] != arr2[i][j]){
                return false;
            }
        }
    }

    return true;
}

function updateMainColor(picker){
    document.getElementById('g1').jscolor.hide();
    var myColor = "rgb(" + Math.round(picker.rgb[0]) + ", " + Math.round(picker.rgb[1]) + ", " + Math.round(picker.rgb[2]) + ")"
    mainColor = myColor.slice();

    if(localStorage.getItem("myColorTheme") === null){
        var tempArr = new Array(7);
    }
    else{
        var tempArr = JSON.parse(localStorage.myColorTheme);
    }

    tempArr[0] = mainColor;
    localStorage.myColorTheme = JSON.stringify(tempArr);
    changeMainColor();
}

function updateTextColor(picker){
    document.getElementById('g2').jscolor.hide();
    var myColor = "rgb(" + Math.round(picker.rgb[0]) + ", " + Math.round(picker.rgb[1]) + ", " + Math.round(picker.rgb[2]) + ")"
    textColor = myColor.slice();

    if(localStorage.getItem("myColorTheme") === null){
        var tempArr = new Array(7);
    }
    else{
        var tempArr = JSON.parse(localStorage.myColorTheme);
    }

    tempArr[1] = textColor;
    localStorage.myColorTheme = JSON.stringify(tempArr);
    changeTextColor();
}

function updateBackgroundColor1(picker){
    document.getElementById('g3').jscolor.hide();
    var myColor = "rgb(" + Math.round(picker.rgb[0]) + ", " + Math.round(picker.rgb[1]) + ", " + Math.round(picker.rgb[2]) + ")"
    bgColor1 = myColor.slice();

    if(localStorage.getItem("myColorTheme") === null){
        var tempArr = new Array(7);
    }
    else{
        var tempArr = JSON.parse(localStorage.myColorTheme);
    }

    tempArr[2] = bgColor1;
    localStorage.myColorTheme = JSON.stringify(tempArr);
    changeBackgroundColor1();
}

function updateBackgroundColor2(picker){
    document.getElementById('g4').jscolor.hide();
    var myColor = "rgb(" + Math.round(picker.rgb[0]) + ", " + Math.round(picker.rgb[1]) + ", " + Math.round(picker.rgb[2]) + ")"
    bgColor2 = myColor.slice();

    if(localStorage.getItem("myColorTheme") === null){
        var tempArr = new Array(7);
    }
    else{
        var tempArr = JSON.parse(localStorage.myColorTheme);
    }

    tempArr[3] = bgColor2;
    localStorage.myColorTheme = JSON.stringify(tempArr);
    changeBackgroundColor2();
}

function updateBackgroundColor3(picker){
    document.getElementById('g5').jscolor.hide();
    var myColor = "rgb(" + Math.round(picker.rgb[0]) + ", " + Math.round(picker.rgb[1]) + ", " + Math.round(picker.rgb[2]) + ")"
    bgColor3 = myColor.slice();

    if(localStorage.getItem("myColorTheme") === null){
        var tempArr = new Array(7);
    }
    else{
        var tempArr = JSON.parse(localStorage.myColorTheme);
    }

    tempArr[4] = bgColor3;
    localStorage.myColorTheme = JSON.stringify(tempArr);
    changeBackgroundColor3();
}

function updateTargetBlockColor(picker){
    document.getElementById('g6').jscolor.hide();
    var myColor = "rgb(" + Math.round(picker.rgb[0]) + ", " + Math.round(picker.rgb[1]) + ", " + Math.round(picker.rgb[2]) + ")"
    targetBlockColor = myColor.slice();

    if(localStorage.getItem("myColorTheme") === null){
        var tempArr = new Array(7);
    }
    else{
        var tempArr = JSON.parse(localStorage.myColorTheme);
    }

    tempArr[5] = targetBlockColor;
    localStorage.myColorTheme = JSON.stringify(tempArr);
    changeTargetBlockColor();
}

function updateBlockColor(picker){
    document.getElementById('g7').jscolor.hide();
    var myColor = "rgb(" + Math.round(picker.rgb[0]) + ", " + Math.round(picker.rgb[1]) + ", " + Math.round(picker.rgb[2]) + ")"
    blockColor = myColor.slice();

    if(localStorage.getItem("myColorTheme") === null){
        var tempArr = new Array(7);
    }
    else{
        var tempArr = JSON.parse(localStorage.myColorTheme);
    }

    tempArr[6] = blockColor;
    localStorage.myColorTheme = JSON.stringify(tempArr);
    changeBlockColor();
}

function playDragSound(){
    dragSound.pause();
    dragSound.currentTime = 0;
    dragSound.play();
}

function playClickSound(){
    clickSound.pause();
    clickSound.currentTime = 0;
    clickSound.play();
}