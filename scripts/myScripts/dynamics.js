function setDynamics(){
    // hand cursor on hover
    
    $(".horizontalDrag").mouseover(function(){
        var css = ".horizontalDrag{cursor: pointer; cursor: grab; cursor: -webkit-grab; cursor: -moz-grab;}";
        var style = document.createElement('style');
        
        if (style.styleSheet) {
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }
        
        document.getElementsByTagName('head')[0].appendChild(style);
    });

    $(".verticalDrag").mouseover(function(){
        var css = ".verticalDrag{cursor: pointer; cursor: grab; cursor: -webkit-grab; cursor: -moz-grab;}";
        var style = document.createElement('style');
        
        if (style.styleSheet) {
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }
        
        document.getElementsByTagName('head')[0].appendChild(style);
    });

    //dragging system
    var size, i;
    var startLeftPos, staticTopPos, prevXpx;
    var collision = false, collisionDirection; // 0 = left, top, 1 = right, bottom;

    $('.horizontalDrag').draggable({
        axis: "x",
        containment: "#game",
        start: function(event, ui){
            possibleScoreLoss = true;
            movesCounter++;
            currentScore -= 10;
            buttonsAvailable = false;
            startLeftPos = getBlockPos(ui.position.left); // in blocks, NOT in pixels!
            staticTopPos = getBlockPos(ui.position.top); // in blocks, NOT in pixels!
            size = $(this).width() / sizeOfBlock; // in blocks, NOT in pixels!
            
            updateReversion(this, ui.position.left, ui.position.top);

            for(i = 0; i < size; i++)
            {
                collisionArray[startLeftPos + i][staticTopPos] = 0;
            }

            prevXpx = ui.position.left;
            playDragSound();
        },
        stop: function(event, ui){
            if(finishingAnim == false){
                var stopLeftPos = getBlockPos(ui.position.left); // in blocks, NOT in pixels!
                var i;   
    
                if(collision == true){
                    if(collisionDirection == 1){
                        stopLeftPos = Math.floor(ui.position.left / sizeOfBlock);
                    }
                    else if(collisionDirection == 0){
                        stopLeftPos = Math.ceil(ui.position.left / sizeOfBlock); 
                    }
                }
    
                for(i = 0; i < size; i++){
                    collisionArray[stopLeftPos + i][staticTopPos] = 1;
                }
    
                testCollisionArray();
    
                blockInMovement = true;
                $(this).animate({left: stopLeftPos * sizeOfBlock}, 70, function(){
                    blockInMovement = false;
    
                    for(i = 0; i < size; i++){
                        if(exitLocationPosition[stopLeftPos + i][staticTopPos] == 1 && hasClass(this, "targetBlock") != false){
                            var direction = (getBlockPos(ui.position.left) == 0) ? "left" : "right";
        
                            levelDone(this, size, direction);
                        }
                    }
                });
                $(this).css("top", staticTopPos * sizeOfBlock);
    
                collision = false;
    
                if(startLeftPos == stopLeftPos){
                    movesCounter--;
                    currentScore += 10;
                }
                else{
                    $("#movesValue").html(movesCounter);
                    $("#scoreValue").html(currentScore);
                }
    
                possibleScoreLoss = false;
                buttonsAvailable = true;
            }  
        },
        drag: function(event, ui){
            if(ui.position.left < prevXpx) {
                console.log('<');
                
                if(Math.abs(ui.position.left - prevXpx) > (sizeOfBlock / 2)){ // if drag diff higher than 50% of block size
                    ui.position.left = prevXpx - (sizeOfBlock / 2);
                }
                
                if(+getBlockPos(ui.position.left) != 0){
                    if(+(collisionArray[Math.ceil(ui.position.left / sizeOfBlock) - 1][staticTopPos]) == 1){
                        console.log("COLLISION");
                        collision = true;
                        collisionDirection = 0;
                        
                        ui.position.left = Math.ceil(ui.position.left / sizeOfBlock) * sizeOfBlock;
                    }
                } 
            }
            else if(ui.position.left > prevXpx) {
                console.log('>');
                
                if(Math.abs(ui.position.left - prevXpx) > (sizeOfBlock / 2)){ // if drag diff higher than 50% of block size
                    ui.position.left = prevXpx + (sizeOfBlock / 2);
                }
                
                if(+(mapSize - Math.floor(ui.position.left / sizeOfBlock) - size) > 0){
                    if(+(collisionArray[Math.floor(ui.position.left / sizeOfBlock) + size][staticTopPos]) == 1){
                        console.log("COLLISION");
                        collision = true;
                        collisionDirection = 1;

                        ui.position.left = Math.floor(ui.position.left / sizeOfBlock) * sizeOfBlock;
                    }
                }     
            }
            else if(ui.position.left == prevXpx && (ui.position.left == 0 || ui.position.left == (mapSize * sizeOfBlock) - (size * sizeOfBlock))){
                console.log("COLLISION WITH BORDER");

                for(i = 0; i < size; i++){
                    if(exitLocationPosition[(ui.position.left / sizeOfBlock) + i][staticTopPos] == 1 && hasClass(this, "targetBlock") != false){
                        var direction = (getBlockPos(ui.position.left) == 0) ? "left" : "right";
                        
                        levelDone(this, size, direction);
                        return false;
                    }
                }
            }

            prevXpx = ui.position.left;
        }
    });

    var startTopPos, staticLeftPos, prevYpx;

    $('.verticalDrag').draggable({
        axis: "y",
        containment: "#game",
        start: function(event, ui){
            possibleScoreLoss = true;
            movesCounter++;
            currentScore -= 10;
            buttonsAvailable = false;
            startTopPos = getBlockPos(ui.position.top); // in blocks, NOT in pixels!
            staticLeftPos = getBlockPos(ui.position.left); // in blocks, NOT in pixels!
            size = $(this).height() / sizeOfBlock; // in blocks, NOT in pixels!

            updateReversion(this, ui.position.left, ui.position.top);

            for(i = 0; i < size; i++)
            {
                collisionArray[staticLeftPos][startTopPos + i] = 0;
            }

            prevYpx = ui.position.top;

            playDragSound();
        },
        stop: function(event, ui){
            if(finishingAnim == false){
                var stopTopPos = getBlockPos(ui.position.top); // in blocks, NOT in pixels!
                
                if(collision == true){
                    if(collisionDirection == 1){
                        stopTopPos = Math.floor(ui.position.top / sizeOfBlock);
                    }
                    else if(collisionDirection == 0){
                        stopTopPos = Math.ceil(ui.position.top / sizeOfBlock); 
                    }
                }
    
                for(i = 0; i < size; i++)
                {
                    collisionArray[staticLeftPos][stopTopPos + i] = 1;
                }
    
                testCollisionArray();
    
                blockInMovement = true;
                $(this).animate({top: stopTopPos * sizeOfBlock}, 70, function(){
                    blockInMovement = false;
    
                    for(i = 0; i < size; i++){
                        if(exitLocationPosition[staticLeftPos][stopTopPos + i] == 1 && hasClass(this, "targetBlock") != false){
                            var direction = (getBlockPos(ui.position.top) == 0) ? "top" : "bottom";
    
                            levelDone(this, size, direction);
                        }
                    }
                });
                $(this).css("left", staticLeftPos * sizeOfBlock);
    
                collision = false;
                
                if(startTopPos == stopTopPos){
                    movesCounter--;
                    currentScore += 10;
                }
                else{
                    $("#movesValue").html(movesCounter);   
                    $("#scoreValue").html(currentScore);
                }
    
                possibleScoreLoss = false;
                buttonsAvailable = true;
            }
        },
        drag: function(event, ui) {
            if(ui.position.top < prevYpx) {
                console.log('^');

                if(Math.abs(ui.position.top - prevYpx) > (sizeOfBlock / 2)){ // if drag diff higher than 50% of block size
                    ui.position.top = prevYpx - (sizeOfBlock / 2);
                }

                if(+getBlockPos(ui.position.top) != 0){
                    if(+(collisionArray[staticLeftPos][Math.ceil(ui.position.top / sizeOfBlock) - 1]) == 1){
                        console.log("COLLISION");
                        collision = true;
                        collisionDirection = 0;
                        
                        ui.position.top = Math.ceil(ui.position.top / sizeOfBlock) * sizeOfBlock;
                    }    
                } 
            }
            else if(ui.position.top > prevYpx) {
                console.log('v');

                if(Math.abs(ui.position.top - prevYpx) > (sizeOfBlock / 2)){ // if drag diff higher than 50% of block size
                    ui.position.top = prevYpx + (sizeOfBlock / 2);
                }

                if(+(mapSize - Math.floor(ui.position.top / sizeOfBlock) - size) > 0){
                    if(+(collisionArray[staticLeftPos][Math.floor(ui.position.top / sizeOfBlock) + size]) == 1){
                        console.log("COLLISION");
                        collision = true;
                        collisionDirection = 1;

                        ui.position.top = Math.floor(ui.position.top / sizeOfBlock) * sizeOfBlock;
                    }
                }
            }
            else if(ui.position.top == prevYpx && (ui.position.top == 0 || ui.position.top == (mapSize * sizeOfBlock) - (size * sizeOfBlock))){
                console.log("COLLISION WITH BORDER");

                for(i = 0; i < size; i++){
                    if(exitLocationPosition[staticLeftPos][(ui.position.top / sizeOfBlock) + i] == 1 && hasClass(this, "targetBlock") != false){
                        var direction = (getBlockPos(ui.position.top) == 0) ? "top" : "bottom";
                        
                        levelDone(this, size, direction);
                        return false;
                    }
                }
            }

            prevYpx = ui.position.top;
        }          
    });

    //clicking system
    $(".horizontalDrag").on("click", function(event){
        var staticTopPos = Math.round(parseInt($(this).css("top")) / sizeOfBlock);
        var posXblock = parseInt($(this).css("left")) / sizeOfBlock + 1;
        var size = $(this).width() / sizeOfBlock;
        var i, j;
        var moved = false;

        updateReversion(this, parseInt($(this).css("left")), parseInt($(this).css("top")));
    
        for(i = 0; i < size; i++)
        {
            collisionArray[posXblock - 1 + i][staticTopPos] = 0;
        }

        //check to left direction
        if(parseInt($(this).css("left")) != 0 && blockInMovement == false){
            if(collisionArray[posXblock - 2][staticTopPos] != 1){
                playDragSound();
                for(i = posXblock - 1; i >= 1; i--){
                    if(collisionArray[i - 1][staticTopPos] == 1){
                        //$(this).css("left", i * sizeOfBlock);
                        blockInMovement = true;
                        $(".horizontalDrag").draggable("disable");
                        $(".verticalDrag").draggable("disable");
                        buttonsAvailable = false;
                        moved = true;
                        incMoveCounter();

                        $(this).animate({left: i * sizeOfBlock}, 100, function(){
                            for(j = 0; j < size; j++)
                            {
                                collisionArray[parseInt($(this).css("left")) / sizeOfBlock + j][staticTopPos] = 1;
                                blockInMovement = false;
                                $(".horizontalDrag").draggable("enable");
                                $(".verticalDrag").draggable("enable");
                                buttonsAvailable = true;
                            }

                            testCollisionArray();
                        });  

                        break;
                    } 
                    if(i == 1){
                        //$(this).css("left", 0);
                        blockInMovement = true;
                        $(".horizontalDrag").draggable("disable");
                        $(".verticalDrag").draggable("disable");
                        buttonsAvailable = false;
                        moved = true;
                        incMoveCounter();

                        $(this).animate({left: 0}, 100, function(){
                            for(j = 0; j < size; j++)
                            {
                                collisionArray[parseInt($(this).css("left")) / sizeOfBlock + j][staticTopPos] = 1;
                                blockInMovement = false;
                                $(".horizontalDrag").draggable("enable");
                                $(".verticalDrag").draggable("enable");
                                buttonsAvailable = true;
                            }

                            testCollisionArray();

                            for(j = 0; j < size; j++){
                                if(exitLocationPosition[0 + j][staticTopPos] == 1 && hasClass(this, "targetBlock") != false){
                                    levelDone(this, size, "left");
                                    return;
                                }
                            }
                        });
                    }  
                }
            }    
        }
        
        //check to right direction
        if((parseInt($(this).css("left")) / sizeOfBlock) + size != mapSize && moved == false && blockInMovement == false){
            if(collisionArray[posXblock + size - 1][staticTopPos] != 1){
                playDragSound();
                for(i = posXblock + size; i <= mapSize; i++){
                    if(collisionArray[i - 1][staticTopPos] == 1){
                        //$(this).css("left", (i - size - 1) * sizeOfBlock);
                        blockInMovement = true;
                        $(".horizontalDrag").draggable("disable");
                        $(".verticalDrag").draggable("disable");
                        buttonsAvailable = false;
                        incMoveCounter();

                        $(this).animate({left: (i - size - 1) * sizeOfBlock}, 100, function(){
                            for(j = 0; j < size; j++)
                            {
                                collisionArray[parseInt($(this).css("left")) / sizeOfBlock + j][staticTopPos] = 1;
                                blockInMovement = false;
                                $(".horizontalDrag").draggable("enable");
                                $(".verticalDrag").draggable("enable");
                                buttonsAvailable = true;
                            }

                            testCollisionArray();
                        });

                        break;
                    }  
                    if(i == mapSize){
                        //$(this).css("left", (i - size) * sizeOfBlock); 
                        blockInMovement = true;
                        $(".horizontalDrag").draggable("disable");
                        $(".verticalDrag").draggable("disable");
                        buttonsAvailable = false;
                        incMoveCounter();

                        $(this).animate({left: (i - size) * sizeOfBlock}, 100, function(){
                            for(j = 0; j < size; j++)
                            {
                                collisionArray[parseInt($(this).css("left")) / sizeOfBlock + j][staticTopPos] = 1;
                                blockInMovement = false;
                                $(".horizontalDrag").draggable("enable");
                                $(".verticalDrag").draggable("enable");
                                buttonsAvailable = true;
                            }

                            testCollisionArray();

                            for(j = 0; j < size; j++){
                                if(exitLocationPosition[((i - 1) - size) + j][staticTopPos] == 1 && hasClass(this, "targetBlock") != false){
                                    levelDone(this, size, "right");
                                    return;
                                }
                            }
                        });
                    }  
                }
            }   
        }

        if(blockInMovement == false){
            for(i = 0; i < size; i++)
            {
                collisionArray[parseInt($(this).css("left")) / sizeOfBlock + i][staticTopPos] = 1;
            }

            testCollisionArray(); 
        }

        moved = false;
    });

    $(".verticalDrag").on("click", function(event){
        var staticLeftPos = Math.round(parseInt($(this).css("left")) / sizeOfBlock);
        var posYblock = parseInt($(this).css("top")) / sizeOfBlock + 1;
        var size = $(this).height() / sizeOfBlock;
        var i;
        var moved = false;

        updateReversion(this, parseInt($(this).css("left")), parseInt($(this).css("top")));
    
        for(i = 0; i < size; i++)
        {
            collisionArray[staticLeftPos][posYblock - 1 + i] = 0;
        }

        //check to top direction
        if(parseInt($(this).css("top")) != 0 && blockInMovement == false){
            if(collisionArray[staticLeftPos][posYblock - 2] != 1){
                playDragSound();
                for(i = posYblock - 1; i >= 1; i--){
                    if(collisionArray[staticLeftPos][i - 1] == 1){
                        //$(this).css("top", i * sizeOfBlock);
                        blockInMovement = true;
                        $(".horizontalDrag").draggable("disable");
                        $(".verticalDrag").draggable("disable");
                        buttonsAvailable = false;
                        moved = true;
                        incMoveCounter();

                        $(this).animate({top: i * sizeOfBlock}, 100,function(){
                            for(j = 0; j < size; j++)
                            {
                                collisionArray[staticLeftPos][parseInt($(this).css("top")) / sizeOfBlock + j] = 1;
                                blockInMovement = false;
                                $(".horizontalDrag").draggable("enable");
                                $(".verticalDrag").draggable("enable");
                                buttonsAvailable = true;
                            }

                            testCollisionArray();
                        });
                        
                        break;
                    } 
                    if(i == 1){
                        //$(this).css("top", 0);
                        blockInMovement = true;
                        $(".horizontalDrag").draggable("disable");
                        $(".verticalDrag").draggable("disable");
                        buttonsAvailable = false;
                        moved = true;
                        incMoveCounter();
                        
                        $(this).animate({top: 0}, 100, function(){
                            for(j = 0; j < size; j++)
                            {
                                collisionArray[staticLeftPos][parseInt($(this).css("top")) / sizeOfBlock + j] = 1;
                                blockInMovement = false;
                                $(".horizontalDrag").draggable("enable");
                                $(".verticalDrag").draggable("enable");
                                buttonsAvailable = true;
                            }

                            testCollisionArray();

                            for(j = 0; j < size; j++){
                                if(exitLocationPosition[staticLeftPos][0 + j] == 1 && hasClass(this, "targetBlock") != false){
                                    levelDone(this, size, "top");
                                    return;
                                }
                            }
                        });
                    }  
                }
            }
        }
        
        //check to bottom direction
        if((parseInt($(this).css("top")) / sizeOfBlock) + size != mapSize && moved == false && blockInMovement == false){
            if(collisionArray[staticLeftPos][posYblock + size - 1] != 1){
                playDragSound();
                for(i = posYblock + size; i <= mapSize; i++){
                    if(collisionArray[staticLeftPos][i - 1] == 1){
                        //$(this).css("top", (i - size - 1) * sizeOfBlock);
                        blockInMovement = true;
                        $(".horizontalDrag").draggable("disable");
                        $(".verticalDrag").draggable("disable");
                        buttonsAvailable = false;
                        incMoveCounter();

                        $(this).animate({top: (i - size - 1) * sizeOfBlock}, 100, function(){
                            for(j = 0; j < size; j++)
                            {
                                collisionArray[staticLeftPos][parseInt($(this).css("top")) / sizeOfBlock + j] = 1;
                                blockInMovement = false;
                                $(".horizontalDrag").draggable("enable");
                                $(".verticalDrag").draggable("enable");
                                buttonsAvailable = true;
                            }

                            testCollisionArray();    
                        });
                        

                        for(j = 0; j < size; j++){
                            if(exitLocationPosition[staticLeftPos][(i - size - 1) + j] == 1 && hasClass(this, "targetBlock") != false){
                                levelDone(this, size, "bottom");
                            }
                        }

                        break;
                    }  
                    if(i == mapSize){
                        //$(this).css("top", (i - size) * sizeOfBlock); 
                        blockInMovement = true;
                        $(".horizontalDrag").draggable("disable");
                        $(".verticalDrag").draggable("disable");
                        buttonsAvailable = false;
                        incMoveCounter();

                        $(this).animate({top: (i - size) * sizeOfBlock}, 100, function(){
                            for(j = 0; j < size; j++)
                            {
                                collisionArray[staticLeftPos][parseInt($(this).css("top")) / sizeOfBlock + j] = 1;
                                blockInMovement = false;
                                $(".horizontalDrag").draggable("enable");
                                $(".verticalDrag").draggable("enable");
                                buttonsAvailable = true;
                            }

                            testCollisionArray();   
                            
                            for(j = 0; j < size; j++){
                                if(exitLocationPosition[staticLeftPos][((i - 1) - size) + j] == 1 && hasClass(this, "targetBlock") != false){
                                    levelDone(this, size, "bottom");
                                    return;
                                }
                            }
                        }); 
                    }  
                }
            } 
        }

        if(blockInMovement == false){
            for(i = 0; i < size; i++)
            {
                collisionArray[staticLeftPos][parseInt($(this).css("top")) / sizeOfBlock + i] = 1;
            }

            testCollisionArray();
        }

        moved = false;  
    });
}