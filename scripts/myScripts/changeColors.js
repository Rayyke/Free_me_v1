var css;
var style;

function changeMainColor(){
    //DOM approach to dynamically drawn divs
    css = ".chooseLevelButton, .chooseLevelButtonDisabled{border-color: " + mainColor + ";}";
    style = document.createElement('style');
    
    if (style.styleSheet) {
        style.styleSheet.cssText = css;
    } else {
        style.appendChild(document.createTextNode(css));
    }
    
    document.getElementsByTagName('head')[0].appendChild(style);

    $("body").animate({color: mainColor}, 350);

    $("#articleIntro").animate({boxShadow: "0px 0px 0px 3px " + mainColor + " inset"}, 350);
    $("#article").animate({boxShadow: "0px 0px 0px 3px " + mainColor + " inset"}, 350);
    $("#articleHelp").animate({boxShadow: "0px 0px 0px 3px " + mainColor + " inset"}, 350);
    $("#articleEnd").animate({boxShadow: "0px 0px 0px 3px " + mainColor + " inset"}, 350);
    
    $(".stat").animate({borderColor: mainColor}, 350);
    $(".line").animate({borderColor: mainColor}, 350);
    $(".articleButtons").animate({borderColor: mainColor}, 350);
    
    $("#game").animate({boxShadow: "0px 0px 0px 2px " + mainColor + " inset"}, 350);
    $("#chooseLevel").animate({boxShadow: "0px 0px 0px 2px " + mainColor + " inset"}, 350);
    $("#levelCompleted").animate({boxShadow: "0px 0px 0px 2px " + mainColor + " inset"}, 350);
    $("#settings").animate({boxShadow: "0px 0px 0px 2px " + mainColor + " inset"}, 350);

    $("#footer").animate({borderColor: mainColor}, 350);
    $("#toogleFooterButton").animate({borderColor: mainColor}, 350);
    $("#currentDateTime").animate({borderColor: mainColor}, 350);
    $(".myDebugField").animate({borderColor: mainColor}, 350);
    $(".footerInputs").animate({borderColor: mainColor}, 350);
    $(".wipeLocalStorage").animate({borderColor: mainColor}, 350);
    $(".showLocalStorage").animate({borderColor: mainColor}, 350);
    $(".picContainer").animate({borderColor: mainColor}, 350); 
}

function changeTextColor(){
    //DOM approach to dynamically drawn divs
    css = ".chooseLevelButtonText, .chooseLevelButtonDisabledText{color: " + textColor + ";}";
    style = document.createElement('style');
    
    if (style.styleSheet) {
        style.styleSheet.cssText = css;
    } else {
        style.appendChild(document.createTextNode(css));
    }
    
    document.getElementsByTagName('head')[0].appendChild(style);

    $("#introBlock1").animate({color: textColor}, 350); 
    $("#introBlock2").animate({color: textColor}, 350);
    $("#introBlock3").animate({color: textColor}, 350);
    $("#introBlock4").animate({color: textColor}, 350);
    $("#helpBlock1").animate({color: textColor}, 350);

    $(".cursiveEnd").animate({color: textColor}, 350); 
    $(".cursiveEnd2").animate({color: textColor}, 350);   
    $("#playAgainButtonText").animate({color: textColor}, 350);    
    $(".stats").animate({color: textColor}, 350);
    $(".buttonsBox").animate({color: textColor}, 350); 
    $("#articleHelpInner").animate({color: textColor}, 350); 
    $("#articleHelpInner2").animate({color: textColor}, 350); 
    $(".arrowContainer").animate({color: textColor}, 350);

    $(".levelCompletedTimeText").animate({color: textColor}, 350);
    $(".levelCompletedScoreText").animate({color: textColor}, 350);
    $(".levelCompletedBestScoreText").animate({color: textColor}, 350);
    $(".levelCompletedMovesText").animate({color: textColor}, 350);
    $("#levelCompletedTimeValue").animate({color: textColor}, 350);
    $("#levelCompletedScoreValue").animate({color: textColor}, 350);
    $("#levelCompletedBestScoreValue").animate({color: textColor}, 350);
    $("#levelCompletedMovesValue").animate({color: textColor}, 350);

    $("#replayButtonText").animate({color: textColor}, 350);
    $("#nextButtonText").animate({color: textColor}, 350);
    $(".settingsBackButtonText").animate({color: textColor}, 350);
    $("#settingsLeft").animate({color: textColor}, 350);

    $("#toogleFooterButton").animate({color: textColor}, 350);
    $("#currentDateTime").animate({color: textColor}, 350);
    $(".myDebugField").animate({color: textColor}, 350);
    $(".footerInputs").animate({color: textColor}, 350);
    $(".wipeLocalStorageText").animate({color: textColor}, 350);
    $(".showLocalStorageText").animate({color: textColor}, 350);
    $("#settingsLeft").animate({color: textColor}, 350);
    $("#settingsLeft").animate({color: textColor}, 350);
    $("#settingsLeft").animate({color: textColor}, 350);
    $("#settingsLeft").animate({color: textColor}, 350);
}

function changeBackgroundColor1(){
    $("body").animate({backgroundColor: bgColor1}, 350);    
}

function changeBackgroundColor2(){
    $("#articleIntro").animate({backgroundColor: bgColor2}, 350); 
    $("#article").animate({backgroundColor: bgColor2}, 350);
    $("#articleHelp").animate({backgroundColor: bgColor2}, 350);
    $("#articleEnd").animate({backgroundColor: bgColor2}, 350);

    $("#introBlock1").animate({backgroundColor: bgColor2}, 350);
    $("#introBlock2").animate({backgroundColor: bgColor2}, 350);
    $("#introBlock3").animate({backgroundColor: bgColor2}, 350); 
    $("#introBlock4").animate({backgroundColor: bgColor2}, 350);
    $("#helpBlock1").animate({backgroundColor: bgColor2}, 350);

    $(".introExit1").animate({backgroundColor: bgColor2}, 350);
    $(".introExit2").animate({backgroundColor: bgColor2}, 350);
    $(".introExit3").animate({backgroundColor: bgColor2}, 350);
    $(".introExit4").animate({backgroundColor: bgColor2}, 350); 

    $("#playAgainButton").animate({backgroundColor: bgColor2}, 350);
    $("#playAgainExit").animate({backgroundColor: bgColor2}, 350);
    $(".stat").animate({backgroundColor: bgColor2}, 350);
    $(".articleButtons").animate({backgroundColor: bgColor2}, 350);

    $("#footer").animate({backgroundColor: bgColor2}, 350);
    $("#toogleFooterButton").animate({backgroundColor: bgColor2}, 350); 
    $("#currentDateTime").animate({backgroundColor: bgColor2}, 350);
    $(".footerInputs").animate({backgroundColor: bgColor2}, 350);
    $(".wipeLocalStorage").animate({backgroundColor: bgColor2}, 350);
    $(".showLocalStorage").animate({backgroundColor: bgColor2}, 350);
    $(".picContainer").animate({backgroundColor: bgColor2}, 350);
}

function changeBackgroundColor3(){
    //DOM approach to dynamically drawn divs
    css = ".chooseLevelButton, .chooseLevelButtonDisabled{background-color: " + bgColor3 + ";}";
    style = document.createElement('style');
    
    if (style.styleSheet) {
        style.styleSheet.cssText = css;
    } else {
        style.appendChild(document.createTextNode(css));
    }
    
    document.getElementsByTagName('head')[0].appendChild(style);

    css = ".exit{background-color: " + bgColor3 + ";}";
    style = document.createElement('style');
    
    if (style.styleSheet) {
        style.styleSheet.cssText = css;
    } else {
        style.appendChild(document.createTextNode(css));
    }
    
    document.getElementsByTagName('head')[0].appendChild(style);

    $("#game").animate({backgroundColor: bgColor3}, 350);  
    $("#chooseLevel").animate({backgroundColor: bgColor3}, 350); 
    $("#levelCompleted").animate({backgroundColor: bgColor3}, 350); 
    $("#settings").animate({backgroundColor: bgColor3}, 350); 
    $(".chooseLevelButton").animate({backgroundColor: bgColor3}, 350); 
    $(".chooseLevelButtonDisabled").animate({backgroundColor: bgColor3}, 350);   
    $("#replayButton").animate({backgroundColor: bgColor3}, 350); 
    $("#nextButton").animate({backgroundColor: bgColor3}, 350); 
    $("#levelCompletedMsgExit1").animate({backgroundColor: bgColor3}, 350); 
    $("#levelCompletedMsgExit2").animate({backgroundColor: bgColor3}, 350); 
    $("#settingsBackButton").animate({backgroundColor: bgColor3}, 350);   
    $(".settingsExit").animate({backgroundColor: bgColor3}, 350); 
}

function changeTargetBlockColor(){ 
    //DOM approach to dynamically drawn divs
    css = ".targetBlockInner{background-color: " + targetBlockColor + ";}";
    style = document.createElement('style');
    
    if (style.styleSheet) {
        style.styleSheet.cssText = css;
    } else {
        style.appendChild(document.createTextNode(css));
    }
    
    document.getElementsByTagName('head')[0].appendChild(style);

    $("#introBlock1").animate({boxShadow: "0px 0px 0px 3px " + targetBlockColor + " inset"}, 350);
    $("#introBlock3").animate({boxShadow: "0px 0px 0px 3px " + targetBlockColor + " inset"}, 350);
    $("#playAgainButton").animate({boxShadow: "0px 0px 0px 2px " + targetBlockColor + " inset"}, 350);
    $("#nextButton").animate({boxShadow: "0px 0px 0px 2px " + targetBlockColor + " inset"}, 350);
}

function changeBlockColor(){
    //DOM approach to dynamically drawn divs
    css = ".blockInner{background-color: " + blockColor + ";}";
    style = document.createElement('style');
    
    if (style.styleSheet) {
        style.styleSheet.cssText = css;
    } else {
        style.appendChild(document.createTextNode(css));
    }
    
    document.getElementsByTagName('head')[0].appendChild(style);

    $("#introBlock2").animate({boxShadow: "0px 0px 0px 3px " + blockColor + " inset"}, 350);
    $("#introBlock4").animate({boxShadow: "0px 0px 0px 3px " + blockColor + " inset"}, 350);
    $("#helpBlock1").animate({boxShadow: "0px 0px 0px 3px " + blockColor + " inset"}, 350);
    $("#replayButton").animate({boxShadow: "0px 0px 0px 2px " + blockColor + " inset"}, 350);
    $("#settingsBackButton").animate({boxShadow: "0px 0px 0px 2px " + blockColor + " inset"}, 350);
}