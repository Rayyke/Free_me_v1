function bindButtons(){
    var css;
    var style;

    // in-game buttons hover
    $(".articleButtons").mouseenter(function() {
        $(this).animate({backgroundColor: mainColor}, 200);
    }).mouseleave(function() {
        $(this).animate({backgroundColor: bgColor2}, 200);  
    });

    //in-game stats hover + sortable

    $(".stat").mouseenter(function() {
        $(this).animate({backgroundColor: mainColor}, 200);
    }).mouseleave(function() {
        $(this).animate({backgroundColor: bgColor2}, 200);  
    });

    $( "#sortable" ).sortable({
        axis: "y"
    });

    // intro buttons hover
    $("#introBlock1").mouseenter(function() {
        $(this).animate({backgroundColor: targetBlockColor}, 200);
    }).mouseleave(function() {
        $(this).animate({backgroundColor: bgColor2}, 200);
    });

    $("#introBlock2").mouseenter(function() {
        $(this).animate({backgroundColor: blockColor}, 200);
    }).mouseleave(function() {
        $(this).animate({backgroundColor: bgColor2}, 200);
    });

    $("#introBlock3").mouseenter(function() {
        $(this).animate({backgroundColor: targetBlockColor}, 200);
    }).mouseleave(function() {
        $(this).animate({backgroundColor: bgColor2}, 200);
    });

    $("#introBlock4").mouseenter(function() {
        $(this).animate({backgroundColor: blockColor}, 200);
    }).mouseleave(function() {
        $(this).animate({backgroundColor: bgColor2}, 200);
    });

    $("#helpBlock1").mouseenter(function() {
        $(this).animate({backgroundColor: blockColor}, 200);
    }).mouseleave(function() {
        $(this).animate({backgroundColor: bgColor2}, 200);
    });

    $("#leftArrow").mouseenter(function() {
        $(this).animate({color: mainColor}, 200);
    }).mouseleave(function() {
        $(this).animate({color: textColor}, 200);
    });

    $("#rightArrow").mouseenter(function() {
        $(this).animate({color: mainColor}, 200);
    }).mouseleave(function() {
        $(this).animate({color: textColor}, 200);
    });

    // level completed buttons
    $("#replayButton").mouseenter(function() {
        $(this).animate({backgroundColor: blockColor}, 200);
    }).mouseleave(function() {
        $(this).animate({backgroundColor: bgColor3}, 200);
    });

    $("#nextButton").mouseenter(function() {
        $(this).animate({backgroundColor: targetBlockColor}, 200);
    }).mouseleave(function() {
        $(this).animate({backgroundColor: bgColor3}, 200);
    });

    $("#playAgainButton").mouseenter(function() {
        $(this).animate({backgroundColor: targetBlockColor}, 200);
    }).mouseleave(function() {
        $(this).animate({backgroundColor: bgColor2}, 200);
    });

    // debug buttons
    $("#setLastLevelPlayedButton").mouseenter(function() {
        $(this).animate({backgroundColor: mainColor}, 200);
    }).mouseleave(function() {
        $(this).animate({backgroundColor: bgColor2}, 200);
    }).click(function(){
        $(".setLastLevelPlayedButtonText").animate({fontSize: 45, opacity: 0.5}, 50, function(){
            $(this).animate({fontSize: 15, opacity: 1}, 50);
        });
    });

    $("#setBestScoreButton").mouseenter(function() {
        $(this).animate({backgroundColor: mainColor}, 200);
    }).mouseleave(function() {
        $(this).animate({backgroundColor: bgColor2}, 200);
    }).click(function(){
        $(".setBestScoreButtonText").animate({fontSize: 45, opacity: 0.5}, 50, function(){
            $(this).animate({fontSize: 15, opacity: 1}, 50);
        });
    });

    $("#toogleFooterButton").mouseenter(function() {
        $(this).animate({backgroundColor: mainColor}, 200);
    }).mouseleave(function() {
        $(this).animate({backgroundColor: bgColor2}, 200);
    });

    $("#setLastLevelPlayed").mouseenter(function() {
        $(this).animate({borderColor: textColor}, 200);
    }).mouseleave(function() {
        $(this).animate({borderColor: mainColor}, 200);
    })

    $("#setBestScore").mouseenter(function() {
        $(this).animate({borderColor: textColor}, 200);
    }).mouseleave(function() {
        $(this).animate({borderColor: mainColor}, 200);
    });

    $(".wipeLocalStorage").mouseenter(function() {
        $(this).animate({backgroundColor: mainColor}, 200);
    }).mouseleave(function() {
        $(this).animate({backgroundColor: bgColor2}, 200);
    }).click(function(){
        $(".wipeLocalStorageText").animate({fontSize: 35, opacity: 0.5}, 50, function(){
            $(this).animate({fontSize: 15, opacity: 1}, 50);
        });
    });

    $(".showLocalStorage").mouseenter(function() {
        $(this).animate({backgroundColor: mainColor}, 200);
    }).mouseleave(function() {
        $(this).animate({backgroundColor: bgColor2}, 200);
    }).click(function(){
        $(".showLocalStorageText").animate({fontSize: 35, opacity: 0.5}, 50, function(){
            $(this).animate({fontSize: 15, opacity: 1}, 50);
        });
    });

    $("#settingsBackButton").mouseenter(function() {
        $(this).animate({backgroundColor: blockColor}, 200);
    }).mouseleave(function() {
        $(this).animate({backgroundColor: bgColor3}, 200);
    });

    $(".footerPic").mouseenter(function() {
        $(this).animate({opacity: 1}, 200);
    }).mouseleave(function() {
        $(this).animate({opacity: 0.5}, 200);  
    });
   
    // in-game buttons key binding
    document.addEventListener("keydown",function(e)
    {
        var key = e.which||e.keyCode;

        switch(key){
            case 68: // d
                if(buttonCheckArray[0] == 0){
                    $("#toogleFooterButton").css("background-color", mainColor).click();
                    buttonCheckArray[0] = 1;
                }
            break;

            case 66: // b
                if(buttonCheckArray[1] == 0){
                    $("#backButton").css("background-color", mainColor).click();
                    buttonCheckArray[1] = 1;
                }
            break;

            case 82: // r
                if(buttonCheckArray[2] == 0){
                    $("#resetButton").css("background-color", mainColor).click();
                    buttonCheckArray[2] = 1;
                }
            break; 

            case 77: // m
                if(buttonCheckArray[3] == 0){
                    $("#menuButton").css("background-color", mainColor).click();
                    buttonCheckArray[3] = 1;
                }
            break; 

            case 83: // s
                if(buttonCheckArray[4] == 0){
                    $("#saveButton").css("background-color", mainColor).click();
                    buttonCheckArray[4] = 1;
                }
            break; 

            case 79: // o
                if(buttonCheckArray[5] == 0){
                    $("#settingsButton").css("background-color", mainColor).click();
                    buttonCheckArray[5] = 1;
                }
            break; 

            case 72: // h
                if(buttonCheckArray[6] == 0){
                    $("#helpButton").css("background-color", mainColor).click();
                    buttonCheckArray[6] = 1;
                }
            break; 

            case 37: // left arrow
                if(buttonCheckArray[7] == 0){
                    $("#leftArrow").css("color", mainColor).click();
                    buttonCheckArray[7] = 1;
                }
            break;

            case 39: // right arrow
                if(buttonCheckArray[8] == 0){
                    $("#rightArrow").css("color", mainColor).click();
                    buttonCheckArray[8] = 1;
                }
            break;
        }
    }); 

    document.addEventListener("keyup",function(e)
    {
        var key = e.which||e.keyCode;
        switch(key){
            case 68: // d
                $("#toogleFooterButton").css("background-color", bgColor2);
                buttonCheckArray[0] = 0;  
            break;
            case 66: // b
                $("#backButton").css("background-color", bgColor2);
                buttonCheckArray[1] = 0;
            break; 

            case 82: // r
                $("#resetButton").css("background-color", bgColor2);
                buttonCheckArray[2] = 0;
            break; 

            case 77: // m
                $("#menuButton").css("background-color", bgColor2);
                buttonCheckArray[3] = 0;
            break; 

            case 83: // s
                $("#saveButton").css("background-color", bgColor2);
                buttonCheckArray[4] = 0;
            break; 

            case 79: // o
                $("#settingsButton").css("background-color", bgColor2);
                buttonCheckArray[5] = 0;
            break; 

            case 72: // h
                $("#helpButton").css("background-color", bgColor2);
                buttonCheckArray[6] = 0;
            break; 

            case 37: // left arrow
                $("#leftArrow").css("color", textColor);
                buttonCheckArray[7] = 0;
            break;

            case 39: // right arrow
                $("#rightArrow").css("color", textColor);
                buttonCheckArray[8] = 0;
            break;
        }
    }); 
}

// intro buttons
function continueClicked(){
    if(selectionInProgress == false){
        menuSong.pause();
        playDragSound();

        selectionInProgress = true;

        $("#settings").css("display", "none");
        $("#settings").css("opacity", "0");
        $("#game").css("display", "block");
        $("#game").css("opacity", "1");

        movesCounter = 0;

        $("#movesValue").html(movesCounter);

        currentLevel = parseInt(localStorage.currentLevelStored);

        if(currentLevel > totalLevels){
            currentLevel = totalLevels;
        }

        load("loadMap", currentLevel);

        var t1 = setTimeout(function(){
            fadeSwap("#articleIntro", "#article"); 
            
            displayFooter();
        }, 200);

        $("#introBlock1").animate({left: 1280}, 350, function(){
            resetIntroButtons();
            selectionInProgress = false;
            buttonsAvailable = true;
        });
    
        $("#game").css("display", "block");
        $("#chooseLevel").css("display", "none");
    }  
}

function newGameClicked(){
    if(selectionInProgress == false){
        selectionInProgress = true;

        menuSong.pause();
        playDragSound();

        $("#settings").css("display", "none");
        $("#settings").css("opacity", "0");
        $("#game").css("display", "block");
        $("#game").css("opacity", "1");

        localStorage.isSaved = "no";
        
        try{
            localStorage.removeItem(mySave);    
        }catch(error){}
        
        movesCounter = 0;

        $("#movesValue").html(movesCounter);
        currentLevel = 1;

        localStorage.currentLevelStored = currentLevel.toString();
        load("loadMap", currentLevel);

        var t1 = setTimeout(function(){
            fadeSwap("#articleIntro", "#article"); 
            
            displayFooter();
        }, 200);

        $("#introBlock2").animate({left: -600}, 350, function(){
            resetIntroButtons();
            selectionInProgress = false;
            buttonsAvailable = true;
            $("#introBlock1").css("display", "none");
            $(".introExit4").css("display", "none");
        });
    
        $("#game").css("display", "block");
        $("#chooseLevel").css("display", "none");
    }  
}

function chooseLevelClicked(){
    if(selectionInProgress == false){
        menuSong.pause();
        playDragSound();

        if(localStorage.getItem("currentLevelStored") === null){
            localStorage.currentLevelStored = 1;
        }

        var i;
        localStorage.isSaved = "no";
        try{
            localStorage.removeItem(mySave);    
        }catch(error){}

        $("#settings").css("display", "none");
        $("#settings").css("opacity", "0");
        $("#chooseLevel").css("display", "block");
        $("#chooseLevel").css("opacity", "1");

        selectionInProgress = true;

        currentScore = 0;
        movesCounter = 0;

        $("#levelValue").html("");
        $("#scoreValue").html(currentScore);
        $("#bestScoreValue").html("0");
        $("#movesValue").html(movesCounter);
    
        var t1 = setTimeout(function(){
            fadeSwap("#articleIntro", "#article");
        }, 200);

        $("#introBlock3").animate({left: 1280}, 350, function(){
            resetIntroButtons();
            selectionInProgress = false;
            buttonsAvailable = true;
        });
        
        $("#game").css("display", "none");
        $("#chooseLevel").css("display", "block").css("opacity", 1); 

        $("#chooseLevel").html("");

        var temp = (localStorage.getItem("currentLevelStored") === null) ? 1 : parseInt(localStorage.currentLevelStored);

        if(temp > totalLevels){
            temp = totalLevels;
        }

        for(i = 1; i <= temp; i++){
            $("#chooseLevel").html($("#chooseLevel").html() + "<div class=\"chooseLevelButton pointerOnHover\"><div class=\"chooseLevelButtonText\">" + i + "</div></div>");  
        }

        for(i = temp + 1; i <= totalLevels; i++){
            $("#chooseLevel").html($("#chooseLevel").html() + "<div class=\"chooseLevelButtonDisabled pointerOnHover\"><div class=\"chooseLevelButtonDisabledText\">" 
            + "<i class=\"fa fa-lock\" aria-hidden=\"true\"></i>" + "</div></div>");  
        }

        $(".chooseLevelButton").mouseenter(function() {
            $(this).animate({backgroundColor: mainColor}, 200);
        }).mouseleave(function() {
            $(this).animate({backgroundColor: bgColor3}, 200);  
        }).click(function(){
            var targetLevel = parseInt($(this).html().replace(/[^0-9]/gi, ''), 10);

            playClickSound();

            load("loadMap", targetLevel);
            fadeSwap("#chooseLevel", "#game");

            displayFooter();
        });

        $(".chooseLevelButtonDisabled").mouseenter(function() {
            $(this).animate({backgroundColor: mainColor}, 200);
        }).mouseleave(function() {
            $(this).animate({backgroundColor: bgColor3}, 200);  
        }).click(function(){
            playClickSound();
        });
    }  
}

function helpClicked(){
    if(selectionInProgress == false){
        selectionInProgress = true;
        whereToBackFromHelp = 0;
        arrowsAvailable = true;

        playDragSound();

        var t1 = setTimeout(function(){
            fadeSwap("#articleIntro", "#articleHelp"); 
        }, 200);

        $("#introBlock4").animate({left: -600}, 350, function(){
            resetIntroButtons();
            selectionInProgress = false;
        });        
    }  
}

function helpBackClicked(){
    if(selectionInProgress == false){
        selectionInProgress = true;
        currentHelpPage = 1;
        arrowsAvailable = false;

        playDragSound();

        var t1 = setTimeout(function(){
            fadeSwap("#articleHelp", ((whereToBackFromHelp == 0) ? "#articleIntro" : "#article")); 
            
            if(whereToBackFromHelp == 1){
                displayFooter();
                buttonsAvailable = true;
            }
        }, 200);

        $("#helpBlock1").animate({left: -600}, 350, function(){
            resetIntroButtons();
            selectionInProgress = false;

            $("#articleHelpInner").css("display", "block").css("opacity", 1);
            $("#articleHelpInner2").css("display", "none").css("opacity", 0);
        });        
    }  
}

function leftArrowClicked(){
    if(arrowsAvailable == true){
        playClickSound();
        arrowsAvailable = false;

        var t1 = setTimeout(function(){
            arrowsAvailable = true;
        }, 200);

        if(currentHelpPage == 1){
            currentHelpPage = 2;
    
            fadeSwap("#articleHelpInner", "#articleHelpInner2");
        }
        else if(currentHelpPage == 2){
            currentHelpPage = 1;
    
            fadeSwap("#articleHelpInner2", "#articleHelpInner");
        }
    }
}

function rightArrowClicked(){
    if(arrowsAvailable == true){
        playClickSound();
        arrowsAvailable = false;

        var t1 = setTimeout(function(){
            arrowsAvailable = true;
        }, 200);

        if(currentHelpPage == 1){
            currentHelpPage = 2;
    
            fadeSwap("#articleHelpInner", "#articleHelpInner2");
        }
        else if(currentHelpPage == 2){
            currentHelpPage = 1;
    
            fadeSwap("#articleHelpInner2", "#articleHelpInner");
        }
    }
}

// in-game panel buttons
function backButtonAction(){
    try{
        if(masterCheck() == true && $("#game").css("display") == "block" && backMovesAvailable > 0 && cmp2DArrays(collisionArray, reversion[3]) == false && movesCounter > 0){
            blockInMovement = true;
            buttonsAvailable = false;
            playClickSound();
    
            var t = setTimeout(function(){
                blockInMovement = false;  
            }, 105);

            $(".backButtonText").animate({fontSize: 72, opacity: 0.5, left: -20, top: -37.5}, 50, function(){
                $(this).animate({fontSize: 24, opacity: 1, left: 4, top: -1.5}, 50, function(){
                    buttonsAvailable = true;
                });
            });
    
            
            $("#backslidesValue").html(--backMovesAvailable);
            playDragSound();
            $(reversion[0]).animate({left: reversion[1]}, 100).animate({top: reversion[2]}, 100);

            for(var i=0; i < mapSize; i++){
                for(var j=0; j < mapSize; j++){
                    collisionArray[i][j] = reversion[3][i][j];
                }
            }
            
            testCollisionArray();
        }
    }catch(error){}
}

function resetButtonAction(){
    if(masterCheck() == true && $("#game").css("display") == "block"){
        playClickSound();
        clearInterval(levelTimer2);
        levelSeconds = 0;
        buttonAvailable = false;
        localStorage.isSaved = "no";

        $(".resetButtonText").animate({fontSize: 72, opacity: 0.5, left: -19.5, top: -38.5}, 50, function(){
            $(this).animate({fontSize: 24, opacity: 1, left: 4.5, top: -2.5}, 50, function(){
                buttonsAvailable = true;
            });
        });

        clearMapContent();
        load("loadMap", currentLevel);    
    }
}

function menuButtonAction(){
    if(masterCheck() == true){
        playClickSound();
        
        menuSong.currentTime = 0;
        menuSong.play();

        $("#introBlock1").css("display", "block");
        $(".introExit4").css("display", "block");

        clearInterval(levelTimer);
        clearInterval(levelTimer2);
        levelSeconds = 0;
        buttonsAvailable = false;

        $(".menuButtonText").animate({fontSize: 60, opacity: 0.5, left: -14.5, top: -29.5}, 50, function(){
            $(this).animate({fontSize: 20, opacity: 1, left: 5.5, top: 0.5}, 50, function(){
                buttonsAvailable = true;
            });
        });

        fadeSwap("#article", "#articleIntro"); 
        
        buttonsAvailable = false;
        hideFooter();
    }
}

function saveButtonAction(){
    if(masterCheck() == true && $("#game").css("display") == "block"){
        playClickSound();
        buttonsAvailable = false;
        localStorage.isSaved = "yes";

        $(".saveButtonText").animate({fontSize: 60, opacity: 0.5, left: -14, top: -30}, 50, function(){
            $(this).animate({fontSize: 20, opacity: 1, left: 6, top: 0}, 50, function(){
                buttonsAvailable = true;
            });
        });  

        /*
            arr2save form -> LEVEL, CURRENT_SCORE, MOVES_COUTNER, BACKSLIDES_AVAILABLE, COLLISION_ARRAY, $(#game).html()
        */

        var arr2save = new Array();

        arr2save.push(currentLevel);
        arr2save.push(currentScore);
        arr2save.push(movesCounter);
        arr2save.push(backMovesAvailable);
        arr2save.push(levelSeconds);

        arr2save[5] = new Array(mapSize);

        for (var i = 0; i < mapSize; i++) {
            arr2save[5][i] = new Array(mapSize);
        }
    
        for(i = 0; i < mapSize; i++){
            for(var j = 0; j < mapSize; j++){
                arr2save[5][i][j] = collisionArray[i][j];
            }
        }

        arr2save.push($("#game").html());
        
        localStorage.mySave = JSON.stringify(arr2save);
    }
}

function settingsButtonAction(){
    if(masterCheck() == true){
        clearInterval(levelTimer2);
        levelSeconds = 0;
        buttonsAvailable = false;
        hideFooter(); 
        playClickSound();

        $(".settingsButtonText").animate({fontSize: 60, opacity: 0.5, left: -16.5, top: -29.5}, 50, function(){
            $(this).animate({fontSize: 20, opacity: 1, left: 3.5, top: 0.5}, 50, function(){
                buttonsAvailable = true;
            });
        });

        if($("#game").css("display") == "block"){
            wasDisplayed = "#game";
            fadeSwap("#game", "#settings");  
        }
        else if($("#chooseLevel").css("display") == "block"){
            wasDisplayed = "#chooseLevel";
            fadeSwap("#chooseLevel", "#settings");    
        } 
    }
}

function helpButtonAction(){
    if(masterCheck() == true){
        playClickSound();
        arrowsAvailable = true;
        whereToBackFromHelp = 1;

        $(".helpButtonText").animate({fontSize: 72, opacity: 0.5, left: -4, top: -34.5}, 50, function(){
            $(this).animate({fontSize: 24, opacity: 1, left: 7, top: 1.5}, 50, function(){
                buttonsAvailable = true;
            });
        });
    
        fadeSwap("#article", "#articleHelp"); 
        
        buttonsAvailable = false;
        hideFooter();  
    }
}

function replayButtonClicked(){
    if(selectionInProgress == false && blockInMovement == false){
        selectionInProgress = true;
        blockInMovement = true;

        playDragSound();

        clearInterval(levelTimer2);
        levelSeconds = 0;
    
        if(currentLevel != totalLevels){
            currentLevel--;
        }
         
        var t1 = setTimeout(function(){
            clearMapContent();
            load("loadMap", currentLevel);
    
            fadeSwap("#levelCompleted", "#game"); 
            displayFooter();
        }, 200);
    
        $("#replayButton").animate({left: -400}, 350, function(){
            resetGameCompletedButtons();
            selectionInProgress = false;
            buttonsAvailable = true;
            blockInMovement = false;
        });   
    }  
}

function nextButtonClicked(){
    if(selectionInProgress == false && blockInMovement == false){
        selectionInProgress = true;
        blockInMovement = true;

        playDragSound();

        clearInterval(levelTimer2);
        levelSeconds = 0;
    
        var t1 = setTimeout(function(){
            clearMapContent();

            if(currentLevel <= totalLevels){
                load("loadMap", currentLevel);
                
                fadeSwap("#levelCompleted", "#game"); 
                displayFooter();
            }   
            else{   
                $(".levelCompletedHeading").html("GAME COMPLETED");
                endSong.pause();
                endSong.currentTime = 0;
                endSong.play();
                fadeSwap("#article", "#articleEnd"); 
            }
        }, 200);
    
        $("#nextButton").animate({left: 1040}, 350, function(){
            resetGameCompletedButtons();
            selectionInProgress = false;
            
            if(currentLevel <= totalLevels){
                buttonsAvailable = true; 
            }   
            else{   
                buttonsAvailable = false; 
            }
            
            blockInMovement = false;
        });   
    }  
}

function playAgainButtonClicked(){
    clearInterval(levelTimer);
    clearInterval(levelTimer2);
    levelSeconds = 0;
    buttonsAvailable = false;

    endSong.pause();
    endSong.currentTime = 0;
    playDragSound();
    menuSong.currentTime = 0;
    menuSong.play();

    $("#levelCompleted").css("display", "none").css("opacity", "0");
    $("#game").css("display", "block").css("opacity", "1");

    var t1 = setTimeout(function(){
        fadeSwap("#articleEnd", "#articleIntro");
    }, 200);

    $("#playAgainButton").animate({left: 1480}, 350, function(){
        buttonsAvailable = true;
        $("#playAgainButton").css("left", "50%");
    });   

    hideFooter();    
}

function showLocalStorage(){
    playClickSound();

    console.log("isSaved: " + localStorage.isSaved);  
    console.log("mySave: " + localStorage.mySave); 
    console.log("currentLevelStored: " + localStorage.currentLevelStored);
    console.log("bestScore: " + localStorage.bestScore);
    console.log("myColorTheme: " + localStorage.myColorTheme);   
}

function deleteLocalStorage(){
    playClickSound();

    localStorage.removeItem("isSaved");
    localStorage.removeItem("mySave");
    localStorage.removeItem("currentLevelStored");
    localStorage.removeItem("bestScore");
    localStorage.removeItem("myColorTheme");

    console.log("All local storage cleared!");
}

function setBestScore(){
    playClickSound();

    if(localStorage.getItem("bestScore") === null){
        var tempArr = new Array(10);
    }
    else{
        var tempArr = JSON.parse(localStorage.bestScore);
    }

    tempArr[currentLevel - 1] = $("#setBestScore").val();
    localStorage.bestScore = JSON.stringify(tempArr);

    $("#bestScoreValue").html($("#setBestScore").val());
}

function setLastLevelPlayed(){
    playClickSound();

    localStorage.currentLevelStored = $("#setLastLevelPlayed").val();
    localStorage.currentLevelStored++;
}

function settingsBackClicked(){
    selectionInProgress = true;
    buttonsAvailable = false;
    blockInMovement = true;
    playDragSound()

    var t1 = setTimeout(function(){
        if(wasDisplayed == "#game"){
            fadeSwap("#settings", "#game");
            displayFooter();
        }
        else if(wasDisplayed == "#chooseLevel"){
            fadeSwap("#settings", "#chooseLevel");
        }
    }, 200);

    $("#settingsBackButton").animate({left: -400}, 350, function(){
        $(this).css("left", "50%");
        selectionInProgress = false;
        buttonsAvailable = true;
        blockInMovement = false;
    });   
}