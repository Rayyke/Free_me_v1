var footerToogled, debugMode;
var selectionInProgress, blockInMovement, buttonCheckArray, buttonsAvailable, arrowsAvailable;
var currentLevel, currentHelpPage, totalLevels, mapSize, movesCounter, backMovesAvailable, currentScore, sizeOfBlock, whereToBackFromHelp, wasDisplayed, possibleScoreLoss, finishingAnim;
var blockSpacing, rightPanelWidth;
var mainColor, textColor, bgColor1, bgColor2, bgColor3, blockColor, targetBlockColor;
var reversion, collisionArray, exitLocationPosition;
var xmlTimeOut, levelTimer, levelTimer2, levelSeconds;
var menuSong, dragSound, clickSound, levelCompletedSound, endSong;

$(document).ready(function(){
    defaultSettings(); // set global variables to default
    load("tryOpen"); // trying to open XML, testing XML availability
    bindButtons(); // binding shortcuts, setting hoverings, and other functionalities 
});

function load(task, value){
    var xhttp = new XMLHttpRequest();
    
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var xmlDoc = this.responseXML;

            if(task == "loadMap"){
                var myArray = new Array();
                var temp = 0, blockCounter = 0;
    
                myArray.push(value);
                myArray.push(xmlDoc.getElementsByTagName("output")[value - 1].childNodes[0].nodeValue);
                myArray.push(parseInt(xmlDoc.getElementsByTagName("mapsize")[value - 1].childNodes[0].nodeValue));
    
                temp = xmlDoc.getElementsByTagName("level")[value - 1].getElementsByTagName("block").length;
                myArray.push(temp);
    
                for(var i = 0; i < value - 1; i++)
                {
                    blockCounter += xmlDoc.getElementsByTagName("level")[i].getElementsByTagName("block").length;       
                }

                for(var j = 0; j < temp; j++){
                    myArray.push(xmlDoc.getElementsByTagName("block")[blockCounter++].childNodes[0].nodeValue);
                }
                
                loadMapContent(myArray);
            }
            else if(task == "tryOpen"){
                clearInterval(xmlTimeOut);
                $("#game").html("");
            }
            else if(task == "countLevels"){
                totalLevels = xmlDoc.getElementsByTagName("level").length;

                var tmpList = document.getElementById("setLastLevelPlayed");
                
                for(var i = 1; i <= totalLevels; i++){
                    var option = document.createElement("option");
                    option.text = "Level " + i;
                    option.value = i;
                    tmpList.add(option);
                }     
            }
        }
    };
    
    xhttp.open("GET", "XML/levels.xml", true);
    xhttp.send();  
}