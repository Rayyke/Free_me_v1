/*
    dataArray form -> LEVEL, MAPSIZE, NUMBER_OF_BLOCKS, TARGET_BLOCK, CASUAL_BLOCK_1, CASUAL_BLOCK_2, ...
*/

function loadMapContent(dataArray){
    if(localStorage.isSaved == "yes"){
        var arr2load = JSON.parse(localStorage.mySave);   
    }

    currentLevel = (localStorage.isSaved == "yes") ? arr2load[0] : dataArray[0];
    currentScore = (localStorage.isSaved == "yes") ? arr2load[1] : 999;
    movesCounter = (localStorage.isSaved == "yes") ? arr2load[2] : 0;
    backMovesAvailable = (localStorage.isSaved == "yes") ? arr2load[3] : 3;
    levelSeconds = (localStorage.isSaved == "yes") ? arr2load[4] : 0;
    mapSize = dataArray[2];
    changeSizeOfBlock(600 / mapSize);
    finishingAnim = false;
    possibleScoreLoss = false;

    if(localStorage.getItem("bestScore") === null){
        $("#bestScoreValue").html("0");    
    }
    else{
        var tempArr = JSON.parse(localStorage.bestScore);

        if(tempArr[currentLevel - 1] == null){
            $("#bestScoreValue").html("0");   
        }
        else{
            $("#bestScoreValue").html(tempArr[currentLevel - 1]);
        }
    }

    $("#scoreValue").html(currentScore);
    $("#movesValue").html(movesCounter);
    $("#backslidesValue").html(backMovesAvailable);

    collisionArray = new Array(mapSize);
    exitLocationPosition = new Array(mapSize);

    $("#levelValue").html(currentLevel);
    $("#movesValue").html(movesCounter);
    $("#game").html("");

    for (var i = 0; i < mapSize; i++) {
        collisionArray[i] = new Array(mapSize);
        exitLocationPosition[i] = new Array(mapSize);
    }
    
    for(i = 0; i < mapSize; i++){
        for(var j = 0; j < mapSize; j++){
            collisionArray[i][j] = 0;
            exitLocationPosition[i][j] = 0;
        }
    }

    drawExit(dataArray[1]);

    if(localStorage.isSaved == "yes"){
        for(i = 0; i < mapSize; i++){
            for(j = 0; j < mapSize; j++){
                collisionArray[i][j] = arr2load[5][i][j];
            }
        }
    }
    
    // drawing blocks
    if(localStorage.isSaved == "no"){
        for(var i = 4; i < dataArray.length; i++)
        {
            if(i == 4){
                drawBlock(dataArray[i], true); // first is always the target block
            }
            else{
                drawBlock(dataArray[i], false);
            }
        }
    }
    else if(localStorage.isSaved == "yes"){
        $("#game").html(arr2load[6]);
    }
    
    if(hitboxToogled == true){
        $(".block").css("background-color", "rgba(255, 0, 0, 0.3)");
    }

    // implementing dragging and clicking system
    setDynamics();

    console.log("Level " + currentLevel + " loaded!");
    
    // display arrays to console
    testCollisionArray();
    testExitLocationPosition();

    buttonsAvailable = true;

    levelTimer = setInterval(function(){
        decScore(1);
    }, 1000);

    
    levelTimer2 = setInterval(function(){
        levelSeconds++;
    }, 1000);
}

// draw block, blockParams contains block params (i.e.: A1:B1), isTargetBlock says if it´s target block or casual one (true/false)
function drawBlock(blockParams, isTargetBlock){
    if(blockParams.length == 5){
        var width = (Math.abs(blockParams.charCodeAt(0) - blockParams.charCodeAt(3)) + 1) * sizeOfBlock;
        var height = (Math.abs(blockParams.charCodeAt(1) - blockParams.charCodeAt(4)) + 1) * sizeOfBlock;
    }
    else if(blockParams.length == 2){
        var width = sizeOfBlock;
        var height = sizeOfBlock;    
    }
    
    var left = (blockParams.charCodeAt(0) - "A".charCodeAt(0)) * sizeOfBlock;
    var top = (blockParams.charCodeAt(1) - "1".charCodeAt(0)) * sizeOfBlock; 

    var widthInner = width - (2 * blockSpacing);
    var heightInner = height - (2 * blockSpacing);
    var leftInner = blockSpacing;
    var topInner = blockSpacing;

    if(blockParams.length == 5){
        //horizontal block
        if(+(blockParams.charCodeAt(0) - blockParams.charCodeAt(3)) != 0 && +(blockParams.charCodeAt(1) - blockParams.charCodeAt(4)) == 0){
            $("#game").html($("#game").html() + "<div class=\"block horizontalDrag " + ((isTargetBlock == true) ? "targetBlock" : "") + "\" style=\"width: " + width + "px; height: " + height + "px; left: " + left + "px; top: " + top + "px;\"><div class=\" " 
            + ((isTargetBlock == true) ? "targetBlockInner" : "blockInner") + "\" style=\"z-index: -1; width: " + widthInner + "px; height: " + heightInner + "px; left: " 
            + leftInner + "px; top: " + topInner + "px;\">" + "</div></div>");    
        } 
        //vertical block
        else if(+(blockParams.charCodeAt(0) - blockParams.charCodeAt(3)) == 0 && +(blockParams.charCodeAt(1) - blockParams.charCodeAt(4)) != 0){
            $("#game").html($("#game").html() + "<div class=\"block verticalDrag " + ((isTargetBlock == true) ? "targetBlock" : "") + "\" style=\"width: " + width + "px; height: " + height + "px; left: " + left + "px; top: " + top + "px;\"><div class=\" " 
            + ((isTargetBlock == true) ? "targetBlockInner" : "blockInner") + "\" style=\"z-index: -1; width: " + widthInner + "px; height: " + heightInner + "px; left: " 
            + leftInner + "px; top: " + topInner + "px;\">" + "</div></div>");    
        } 
        else{
            console.log("INVALID BLOCK FOUND [" + blockParams + "]");
            return;
        }
    }
    //block 1x1, undraggable
    else if(blockParams.length == 2){
        $("#game").html($("#game").html() + "<div class=\"block\" style=\"width: " + width + "px; height: " + height + "px; left: " + left + "px; top: " + top + "px;\"><div class=\" " 
        + "blockInner" + "\" style=\"z-index: -1; width: " + widthInner + "px; height: " + heightInner + "px; left: " + leftInner + "px; top: " + topInner + "px;\">" + "</div></div>"); 
    }
    else{
        console.log("INVALID BLOCK FOUND [" + blockParams + "]");
        return;
    }
    
    addBlockToCollisionArray(blockParams);
}

function addBlockToCollisionArray(blockParams){
    var size; 

    if(blockParams.length == 5){
        //vertical block
        if(blockParams.charCodeAt(0) == blockParams.charCodeAt(3) && blockParams.charCodeAt(1) != blockParams.charCodeAt(4)){
            size = blockParams.charCodeAt(4) - blockParams.charCodeAt(1) + 1;

            for(var i = 0; i < size; i++){
                collisionArray[blockParams.charCodeAt(0) - "A".charCodeAt(0)][blockParams.charCodeAt(1) - "1".charCodeAt(0) + i] = 1;
            }
        }
        //horizontal block
        else if(blockParams.charCodeAt(0) != blockParams.charCodeAt(3) && blockParams.charCodeAt(1) == blockParams.charCodeAt(4)){
            size = blockParams.charCodeAt(3) - blockParams.charCodeAt(0) + 1;

            for(var i = 0; i < size; i++){
                collisionArray[blockParams.charCodeAt(0) - "A".charCodeAt(0) + i][blockParams.charCodeAt(1) - "1".charCodeAt(0)] = 1;
            }
        }
    }
    //block 1x1
    else if(blockParams.length == 2){
        collisionArray[blockParams.charCodeAt(0) - "A".charCodeAt(0)][blockParams.charCodeAt(1) - "1".charCodeAt(0)] = 1;
    }
}

function drawExit(positionInput){
    if(positionInput.charCodeAt(0) == "A".charCodeAt(0) || positionInput.charCodeAt(0) == "A".charCodeAt(0) + (mapSize - 1)){
        var width = 4;
        var height = sizeOfBlock + 2;

        if(positionInput.charCodeAt(0) == "A".charCodeAt(0)){
            var left = -1;
            var top = ((positionInput.charCodeAt(1) - "1".charCodeAt(0)) * sizeOfBlock) - 1;    
        }
        else if(positionInput.charCodeAt(0) == "A".charCodeAt(0) + (mapSize - 1)){
            var left = mapSize * sizeOfBlock - 3;
            var top = ((positionInput.charCodeAt(1) - "1".charCodeAt(0)) * sizeOfBlock) - 1;      
        }
    }
    else if(positionInput.charCodeAt(1) == "1".charCodeAt(0) || positionInput.charCodeAt(1) == "1".charCodeAt(0) + (mapSize - 1)){
        var width = sizeOfBlock + 2;
        var height = 4;

        if(positionInput.charCodeAt(1) == "1".charCodeAt(0)){
            var left = ((positionInput.charCodeAt(0) - "A".charCodeAt(0)) * sizeOfBlock) - 1;
            var top = -1;    
        }
        else if(positionInput.charCodeAt(1) == "1".charCodeAt(0) + (mapSize - 1)){
            var left = ((positionInput.charCodeAt(0) - "A".charCodeAt(0)) * sizeOfBlock) - 1;
            var top = mapSize * sizeOfBlock - 3;      
        }
    }

    $("#game").html($("#game").html() + "<div class=\"exit\" style=\"z-index: -1; position: absolute; width: " + width + "px; height: " + height + "px; left: " + left + "px; top: " + top + "px;\"></div>");

    addExit(positionInput);
}

function addExit(positionInput){
    exitLocationPosition[positionInput.charCodeAt(0) - "A".charCodeAt(0)][positionInput.charCodeAt(1) - "1".charCodeAt(0)] = 1;
}

function clearMapContent(){
    clearInterval(levelTimer);
    $("#game").html("");

    for(var i = 0; i < mapSize; i++){
        for(var j = 0; j < mapSize; j++){
            collisionArray[i][j] = 0;
            exitLocationPosition[i][j] = 0;
        }
    }

    $("#movesValue").html(0);
    movesCounter = 0;
}